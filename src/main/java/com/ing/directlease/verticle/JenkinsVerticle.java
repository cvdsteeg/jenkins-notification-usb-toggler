package com.ing.directlease.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class JenkinsVerticle extends AbstractVerticle {
    private static Logger LOG = LoggerFactory.getLogger(JenkinsVerticle.class);

    private int PORT;
    private String HOST;

    private String USB_HUB_APP;
    private static final String BLUE = "1";
    private static final String RED = "2";
    private static final String YELLOW = "3";

    private static final String BUILD = "build";
    private static final String STATUS = "status";

    private final Runnable BLUE_ON = () -> execute("-u", "-d", "-d");
    private final Runnable RED_ON = () -> execute("-d", "-u", "-d");
    private final Runnable YELLOW_ON = () -> execute("-d", "-d", "-u");
    private Map<String, Runnable> functionMap = new HashMap<>();

    @Override
    public void start(Future<Void> fut) throws UnknownHostException {
        init();

        vertx
                .createHttpServer()
                .requestHandler(r -> {
                    r.bodyHandler(buffer -> handle(buffer.toJsonObject()));
                    r.response().end();
                })
                .listen(PORT, HOST, result -> {
                    if (result.succeeded()) {
                        fut.complete();
                    } else {
                        fut.fail(result.cause());
                    }
                });
    }

    private void init() throws UnknownHostException {
        LOG.info("context {0}: ", context);
        LOG.info("config {0}: ", config().encodePrettily());

        final String host = java.net.InetAddress.getLocalHost().getCanonicalHostName();
        HOST = config().getString("http.host", host);
        PORT = config().getInteger("http.port", 5500);
        USB_HUB_APP = config().getString("executable.path");

        final String oneOn = config().getString("status.one-on", "SUCCESS");
        final String twoOn = config().getString("status.two-on", "FAILED");
        final String threeOn = config().getString("status.three-on", "PENDING");
        LOG.info("{0} {1} {2} {3} {4} {5}", PORT, HOST, oneOn, twoOn, threeOn, USB_HUB_APP);

        functionMap.put(oneOn, BLUE_ON);
        functionMap.put(twoOn, RED_ON);
        functionMap.put(threeOn, YELLOW_ON);
    }

    private void handle(JsonObject notificationObject) {
        final JsonObject build = notificationObject.getJsonObject(BUILD);
        LOG.info(build);

        final String status = build.getString(STATUS);
        if (status != null) {
            final String phase = build.getString("phase");
            LOG.info("Phase: {0}, status: {1}", phase, status);
            functionMap.get(status).run();
        } else{
            functionMap.get("PENDING").run();
        }
    }

    private void execute(String toggleOne, String toggleTwo, String toggleThree) {
        try {
            Runtime.getRuntime().exec(new String[]{USB_HUB_APP, toggleOne, BLUE});
            Thread.sleep(10);
            Runtime.getRuntime().exec(new String[]{USB_HUB_APP, toggleTwo, RED});
            Thread.sleep(10);
            Runtime.getRuntime().exec(new String[]{USB_HUB_APP, toggleThree, YELLOW});
            LOG.info("Executed {0} with parameters {1} {2} | {3} {4} | {5} {6}", USB_HUB_APP, toggleOne, BLUE, toggleTwo, RED, toggleThree, YELLOW);
        } catch (IOException | InterruptedException e) {
            LOG.error("Unable to execute the app {0}: {1} {2}", USB_HUB_APP, e.getMessage(), Arrays.toString(e.getStackTrace()));
        }
    }
}
