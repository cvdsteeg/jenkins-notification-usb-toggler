import com.ing.directlease.verticle.JenkinsVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.net.URISyntaxException;
import java.net.UnknownHostException;

public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(final String... args) {

        Vertx vertx = Vertx.vertx();

        DeploymentOptions options = null;
        try {
            options = getOptions();
        } catch (URISyntaxException | UnknownHostException e) {
            e.printStackTrace();
        }
        vertx.deployVerticle(JenkinsVerticle.class.getName(), options);
    }

    private static DeploymentOptions getOptions() throws URISyntaxException, UnknownHostException {
        final JsonObject jsonConfig = new JsonObject()
                .put("http.port", 5500)
                .put("executable.path", System.getProperty("user.dir") + "\\usb-app\\ykushcmd.exe")
                .put("status.one-on", "SUCCESS")
                .put("status.two-on", "FAILURE")
                .put("status.three-on", "PENDING");

        return new DeploymentOptions().setConfig(jsonConfig);
    }
}
